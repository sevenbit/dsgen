(defproject dsgen "0.1.0-SNAPSHOT"
  :description "Program generates xml-dataset for java unit tests with dbunit integration by real db-schema"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/java.jdbc "0.2.3"]
                 [postgresql "9.1-901.jdbc4"]]
  :main dsgen.core)
