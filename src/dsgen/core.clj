(ns dsgen.core
  (:gen-class :main true)
  (:require [clojure.java.jdbc :as sql])
  (:use [clojure.java.io]))

(def db-conn {:classname "org.postgresql.Driver"
              :subprotocol "postgresql"
              :subname "//localhost:5432/rmis_food"
              :user "rmis_food"
              :password "rmis_food"})

(defn execute-query
  "Executes sql query an returns result as vector of maps"
  [query]
  (sql/with-connection db-conn
    (sql/with-query-results results
      [query]
      (into [] results))))

(defn gen-xml-element 
  [table, element]
  (str "<" 
       table 
       (clojure.string/join
         (map #(str "\n\t" (name (key %)) "=\"" (val %) \") 
              element))
       "/>\n"))
      
(defn gen-xml [table, data]
  "[{} {} {}] -> xml"
  (map (partial gen-xml-element table) data))

(defn write-file! [output]
  (with-open [wrtr (writer "result.xml")]
    (.write wrtr output)))

(defn -main
  [& args]
  (let [args-count (count args)]
  (if (= 2 args-count)
    (let [table (first args)
          query (second args)]
      (write-file! 
        (clojure.string/join 
          (gen-xml table (execute-query query)))))
    (print "invalid arguments count, must be 2"))))

(defn yeah-test []
  (-main "products"  "select * from products where id = 176 OR id = 177"))

