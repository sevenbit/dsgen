# dsgen
Program generates xml-file with query results content.

## Installation

See core.clj

## Usage

java -jar dsgen-0.1.0-standalone.jar "table-name" "query"

## Examples

java -jar dsgen "products" "select * from products"

generates result.xml.

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License
Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
